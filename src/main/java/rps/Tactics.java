package rps;

public enum Tactics {

  ALWAYS_ROCK,
  ALWAYS_PAPER,
  ALWAYS_SCISSORS,
  SHUFFLE
}
