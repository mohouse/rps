package rps;

import lombok.AllArgsConstructor;

import java.util.Random;

@AllArgsConstructor
class Player {

  private Tactics tactics;

  RockPaperScissorsOption shakeFingers() {
    switch (tactics) {
      case ALWAYS_ROCK:
        return RockPaperScissorsOption.ROCK;

      case ALWAYS_PAPER:
        return RockPaperScissorsOption.PAPER;

      case ALWAYS_SCISSORS:
        return RockPaperScissorsOption.SCISSORS;

      default:
        return RockPaperScissorsOption.values()[new Random().nextInt(RockPaperScissorsOption.values().length)];
    }
  }
}
