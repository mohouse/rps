package rps;

import java.util.Optional;
import java.util.stream.IntStream;

import static rps.RockPaperScissorsOption.getWinner;

public class Game {

  public void play(int numberOfRounds) {

    Player player1 = new Player(Tactics.ALWAYS_ROCK);
    Player player2 = new Player(Tactics.SHUFFLE);

    IntStream.rangeClosed(1, numberOfRounds)
            .forEach(i -> {
              Optional<Player> winner = findWinnerForNewRound(player1, player2);
              if (winner.isEmpty()) {
                System.out.println("There was no winner in round " + i);
              } else if (winner.get() == player1) {
                System.out.println("There winner of round " + i + " is player 1");
              } else {
                System.out.println("There winner of round " + i + " is player 2");
              }
            });
  }

  private Optional<Player> findWinnerForNewRound(Player player1, Player player2) {

    RockPaperScissorsOption optionPlayer1 = player1.shakeFingers();
    RockPaperScissorsOption optionPlayer2 = player2.shakeFingers();
    Optional<RockPaperScissorsOption> winner = getWinner(optionPlayer1, optionPlayer2);
    if (winner.isEmpty()) {
      return Optional.empty();
    }
    if (winner.get() == optionPlayer1) {
      return Optional.of(player1);
    }
    return Optional.of(player2);
  }

  public static void main(String[] args) {
    new Game().play(100);
  }
}
