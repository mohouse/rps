package rps;

import java.util.Optional;

public enum RockPaperScissorsOption {

  ROCK,
  PAPER,
  SCISSORS;

  public static Optional<RockPaperScissorsOption> getWinner(RockPaperScissorsOption option1, RockPaperScissorsOption option2) {
    if (option1 == option2) {
      return Optional.empty();
    }
    switch (option1) {
      case ROCK:
        return option2 == PAPER ? Optional.of(option2) : Optional.of(option1);
      case PAPER:
        return option2 == SCISSORS ? Optional.of(option2) : Optional.of(option1);
      default:
        return option2 == ROCK ? Optional.of(option2) : Optional.of(option1);
    }
  }
}
