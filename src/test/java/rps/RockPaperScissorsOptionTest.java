package rps;

import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static rps.RockPaperScissorsOption.*;

class RockPaperScissorsOptionTest {

  @Test
  void paperShouldWinRock() {
    Optional<RockPaperScissorsOption> winner = getWinner(ROCK, PAPER);
    assertThat(winner.isPresent()).isTrue();
    assertThat(winner.get()).isEqualTo(PAPER);
  }

  @Test
  void rockShouldWinScissors() {
    Optional<RockPaperScissorsOption> winner = getWinner(ROCK, SCISSORS);
    assertThat(winner.isPresent()).isTrue();
    assertThat(winner.get()).isEqualTo(ROCK);
  }

  @Test
  void scissorsShouldWinPaper() {
    Optional<RockPaperScissorsOption> winner = getWinner(PAPER, SCISSORS);
    assertThat(winner.isPresent()).isTrue();
    assertThat(winner.get()).isEqualTo(SCISSORS);
  }

  @Test
  void sameChoiceShouldNotHaveWinner() {
    Optional<RockPaperScissorsOption> winner = getWinner(PAPER, PAPER);
    assertThat(winner.isPresent()).isFalse();
    winner = getWinner(ROCK, ROCK);
    assertThat(winner.isPresent()).isFalse();
    winner = getWinner(SCISSORS, SCISSORS);
    assertThat(winner.isPresent()).isFalse();
  }
}