package rps;

import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;

import static java.util.Arrays.asList;
import static org.assertj.core.api.Java6Assertions.assertThat;
import static rps.RockPaperScissorsOption.*;
import static rps.Tactics.*;

class PlayerTest {

  @Test
  void alwaysRockShouldAlwaysPlayRock() {
    Player player = new Player(ALWAYS_ROCK);
    RockPaperScissorsOption option = player.shakeFingers();
    assertThat(option).isEqualTo(ROCK);
  }

  @Test
  void alwaysScissorsShouldAlwaysPlayScissors() {
    Player player = new Player(ALWAYS_SCISSORS);
    RockPaperScissorsOption option = player.shakeFingers();
    assertThat(option).isEqualTo(SCISSORS);
  }

  @Test
  void alwaysPaperShouldAlwaysPlayPaper() {
    Player player = new Player(ALWAYS_PAPER);
    RockPaperScissorsOption option = player.shakeFingers();
    assertThat(option).isEqualTo(PAPER);
  }

  @Test
  void shuffleShouldPlayAllOptions() {
    Player player = new Player(SHUFFLE);
    Set<RockPaperScissorsOption> result = new HashSet<>();
    while (!result.containsAll(asList(RockPaperScissorsOption.values()))) {
      result.add(player.shakeFingers());
    }
  }

}